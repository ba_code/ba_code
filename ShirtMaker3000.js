/*
 * ShirtMaker3000Variants:
 *  1 - generator for individual client
 *  2 - generator for user shop products
 *  3 - generator for a purchase from user shop
 * */

var ShirtMaker3000 = {

    printSizes: undefined,
//  [
//        {id: 1, name: 'A4', price: 5, width: 21, height: 30 },
//        {id: 2, name: 'A3', price: 10, width: 30, height: 42 },
//        {id: 3, name: 'A3+', price: 15, width: 33, height: 48 },
//        {id: 4, name: 'XXL', price: 20, width: 42, height: 60 }
//    ],

    editPositionCorrector: {top: 1, left: -2},

    variant: 1,

    allowedImageFileTypes: ['jpeg', 'jpg', 'gif', 'png'],

    cmToPxMultiplayer: 59,

    shirtsMultiplayers: undefined,

    shirts: undefined,

    shirtMaker3000DefaultConfig: {
        maxAreaWidth: '200px', //max working area
        maxAreaHeight: '200px',
        maxAreaTop: '20px',
        maxAreaLeft: '20px',

        editBoxWidth: '150px',
        editBoxHeight: '214px',
        editBoxTop: '0px',
        editBoxLeft: '25px'
    },

    defaultControlls: {
        transparentCorners: false
    },

    sideConfig: {},

    loadSM: undefined,

    overprint: undefined,

    forWhom: [],
    shirtKind: [],
    sizes: [],
    colors: [],
    quality: false,
    economy: false,

    side: 'front', //front, back
    filter: {printSize: 0},
    price: 0,
    selectedShirt: undefined,

    history: {
        current: undefined,
        list: [],
        state: [],
        index: 0,
        index2: 0,
        action: false,
        refresh: true
    },

    config: {},
    canvas: undefined,
    textMenu: {
        val: undefined,
        color: undefined,
        align: undefined,
        textDecoration: undefined,
        fontStyle: undefined,
        fontWeight: undefined,
        fontFamily: undefined,
        fontSize: undefined
    },

    buyV3: function () {
        var box = $(this.config.id + ' .editBox');
        var otherSide = this.side == 'front' ? 'back' : 'front';
        this.sideConfig[otherSide] = this.sideConfig[otherSide] || {canvas: {"objects": [], "background": "", "x": 0, "y": 0}};

        var form = $('#generateFormV3');
        form.append($('<input type="hidden" name="pid" value="' + ShirtMaker3000.config.now + '" />'));
        form.append($('<input type="hidden" name="' + this.side + "X" + '" value="' + box.position().left + '" />'));
        form.append($('<input type="hidden" name="' + this.side + "Y" + '" value="' + box.position().top + '" />'));
        form.append($('<input type="hidden" name="' + this.side + "W" + '" value="' + box.width() + '" />'));
        form.append($('<input type="hidden" name="' + this.side + "H" + '" value="' + box.height() + '" />'));
        form.append($('<input type="hidden" name="' + otherSide + "X" + '" value="' + (this.sideConfig[otherSide].x || this.variantConfig[this.side].x - ShirtMaker3000.selectedShirt['left' + (this.side == 'front' ? 'Front' : 'Back')]) + '" />'));
        form.append($('<input type="hidden" name="' + otherSide + "Y" + '" value="' + (this.sideConfig[otherSide].y || this.variantConfig[this.side].y - ShirtMaker3000.selectedShirt['top' + (this.side == 'front' ? 'Front' : 'Back')]) + '" />'));
        form.append($('<input type="hidden" name="' + otherSide + "W" + '" value="' + (this.sideConfig[otherSide].w || this.variantConfig[otherSide].w) + '" />'));
        form.append($('<input type="hidden" name="' + otherSide + "H" + '" value="' + (this.sideConfig[otherSide].h || this.variantConfig[otherSide].h) + '" />'));
        form.append($('<input type="hidden" name="' + otherSide + 'Size" value="' + this.printSizes[this.sideConfig[otherSide].printSize || 0].id + '" />'));
        form.append($('<input type="hidden" name="' + this.side + 'Size" value="' + this.printSizes[this.filter.printSize || 0].id + '" />'));
        form.append($('<input type="hidden" name="size" value="' + this.filter.size + '" />'));
        form.append($('<input type="hidden" name="frontPrinterSize" value="' + (this.frontPrinterSize || -1) + '" />'));
        form.append($('<input type="hidden" name="backPrinterSize" value="' + (this.backPrinterSize || -1) + '" />'));
        form.append($('<input type="hidden" name="variantid" value="' + this.selectedShirt.selectedVariant.id + '" />'));
        form.append($('<input type="hidden" name="price" value="' + this.price + '" />'));
        form.submit();
    },

    loadFontInfo: function () {
        $.ajax({
            url: "/dane_o_fontach.php",
            type: "GET",
            processData: false,
            contentType: false
        }).done(function (data) {
            ShirtMaker3000.fonts = data;
        });
    },

    generate: function () {
        ShirtMaker3000.loadShirtSpinner('show');

        if (ShirtMaker3000.overprint) {
            if (ShirtMaker3000.side.match(/front/gi))
                ShirtMaker3000.canvas.remove(ShirtMaker3000.overprint.frontImg);
            if (ShirtMaker3000.side.match(/back/gi))
                ShirtMaker3000.canvas.remove(ShirtMaker3000.overprint.backImg);
        }

        var canvasFrontJsonStringify = !this.isCanvasEmpty('front') ? this.getSideCanvasJson('front') : '';
        var canvasBackJsonStringify = !this.isCanvasEmpty('back') ? this.getSideCanvasJson('back') : '';
        var canvasFrontJson = !this.isCanvasEmpty('front') ? this.getSideCanvas('front') : undefined;
        var canvasBackJson = !this.isCanvasEmpty('back') ? this.getSideCanvas('back') : undefined;
        var canvasFront = undefined;
        var canvasBack = undefined;

        var box = $(ShirtMaker3000.config.id + ' .editBox');

        var frontW = frontH = backW = backH = 0;

        var canvasesToLoad = !!canvasFrontJson + !!canvasBackJson;

        var now = new Date().getTime();
        if (canvasFrontJson) {
            frontW = this.side.match(/front/gi) ? box.width() : this.sideConfig['front'].w;
            frontH = this.side.match(/front/gi) ? box.height() : this.sideConfig['front'].h;
            $('#invisibleTemplate').append('<canvas id="canvasTemplateFront_' + now + '" width="' + frontW + '" height="' + frontH + '"></canvas>');
            canvasFront = new fabric.Canvas('canvasTemplateFront_' + now);
            canvasFront.loadFromJSON(canvasFrontJsonStringify, loadedCanvas);
        }
        if (canvasBackJson) {
            backW = this.side.match(/back/gi) ? box.width() : this.sideConfig['back'].w;
            backH = this.side.match(/back/gi) ? box.height() : this.sideConfig['back'].h;
            $('#invisibleTemplate').append('<canvas id="canvasTemplateBack_' + now + '" width="' + backW + '" height="' + backH + '"></canvas>');
            canvasBack = new fabric.Canvas('canvasTemplateBack_' + now);
            canvasBack.loadFromJSON(canvasBackJsonStringify, loadedCanvas);
        }

        function loadedCanvas() {
            if (canvasesToLoad == 0) {
                ShirtMaker3000.loadShirtSpinner('hide');
                return;
            } else if (canvasesToLoad == 1) {
                var data = {front: {left: '', top: '', bottom: '', right: ''}, back: {left: '', top: '', bottom: '', right: ''}};

                if (ShirtMaker3000.overprint) {
                    if (canvasFront) {
                        canvasFront.setWidth(frontW);
                        canvasFront.setHeight(frontH);
                        data.front = {left: 0, top: 0, right: frontW, bottom: frontH};

                        canvasFront.add(ShirtMaker3000.overprint.frontImg);
                        canvasFront.renderAll();
                    }
                    if (canvasBack) {
                        canvasBack.setWidth(backW);
                        canvasBack.setHeight(backH);
                        data.back = {left: 0, top: 0, right: backW, bottom: backH};

                        canvasBack.add(ShirtMaker3000.overprint.backImg);
                        canvasBack.renderAll();
                    }
                } else {
                    if (canvasFront) data.front = ShirtMaker3000.optimizeSideCanvas('front', canvasFront, frontW, frontH);
                    if (canvasBack) data.back = ShirtMaker3000.optimizeSideCanvas('back', canvasBack, backW, backH);
                }

                var fd = new FormData();
                fd.append("action", 'um');
                fd.append("pid", ShirtMaker3000.config.now);
                if (canvasFront) fd.append("front", $('#canvasTemplateFront_' + now)[0].toDataURL());
                if (canvasBack) fd.append("back", $('#canvasTemplateBack_' + now)[0].toDataURL());

                if (ShirtMaker3000.overprint) {
                    if (canvasFront) {
                        data.front = ShirtMaker3000.optimizeSideCanvas('front', canvasFront, frontW, frontH);
                        canvasFront.remove(ShirtMaker3000.overprint.frontImg);
                        canvasFront.renderAll();
                    }
                    if (canvasBack) {
                        data.back = ShirtMaker3000.optimizeSideCanvas('back', canvasBack, backW, backH);
                        canvasBack.remove(ShirtMaker3000.overprint.backImg);
                        canvasBack.renderAll();
                    }
                }

                try {
                    if(!ShirtMaker3000.isCanvasEmpty('front'))
                    {
                        var allObjects = canvasFront.getObjects();
                        for (var idx = 0; idx < allObjects.length;) {
                            var el = allObjects[idx];
                            if(el.type == 'text' && !el.text)
                                canvasFront.remove(el); //remove empty texts
                            else
                                ++idx;
                        }
                    }
                } catch (e) {}

                var frontRecipe = ShirtMaker3000.isCanvasEmpty('front') ? '' : JSON.stringify(canvasFront).replace(/\\/g, '\\\\').replace(/\'/g, '\\\'').replace(/"/g, '\\"');
                var backRecipe = ShirtMaker3000.isCanvasEmpty('back') ? '' : JSON.stringify(canvasBack).replace(/\\/g, '\\\\').replace(/\'/g, '\\\'').replace(/"/g, '\\"');

                $.ajax({
                    url: "/ShirtMaker3000.php",
                    type: "POST",
                    data: fd,
                    processData: false,
                    contentType: false
                }).done(function (result) {
                    var box = $(ShirtMaker3000.config.id + ' .editBox');
                    var otherSide = ShirtMaker3000.side == 'front' ? 'back' : 'front';
                    ShirtMaker3000.sideConfig[otherSide] = ShirtMaker3000.sideConfig[otherSide] || {canvas: {"objects": [], "background": "", "x": 0, "y": 0}};

                    var form = $('#generateFormV' + ShirtMaker3000.variant);
                    if(ShirtMaker3000.variantConfig && ShirtMaker3000.variantConfig.edit)
                        form.append($('<input type="hidden" name="edit" value="' + ShirtMaker3000.variantConfig.edit + '" />'));
                    form.append($('<input type="hidden" name="pid" value="' + ShirtMaker3000.config.now + '" />'));
                    form.append($('<input type="hidden" name="' + ShirtMaker3000.side + "X" + '" value="' + (ShirtMaker3000.isCanvasEmpty(ShirtMaker3000.side) ? '-1' : (box.position().left + data[ShirtMaker3000.side].left)) + '" />'));
                    form.append($('<input type="hidden" name="' + ShirtMaker3000.side + "Y" + '" value="' + (ShirtMaker3000.isCanvasEmpty(ShirtMaker3000.side) ? '-1' : (box.position().top + data[ShirtMaker3000.side].top)) + '" />'));
                    form.append($('<input type="hidden" name="' + otherSide + "X" + '" value="' + (ShirtMaker3000.isCanvasEmpty(otherSide) ? '-1' : ((ShirtMaker3000.sideConfig[otherSide].x || 0) + data[otherSide].left)) + '" />'));
                    form.append($('<input type="hidden" name="' + otherSide + "Y" + '" value="' + (ShirtMaker3000.isCanvasEmpty(otherSide) ? '-1' : ((ShirtMaker3000.sideConfig[otherSide].y || 0) + data[otherSide].top)) + '" />'));
                    form.append($('<input type="hidden" name="frontExists" value="' + (ShirtMaker3000.isFrontCanvasEmpty() ? "false" : "true" ) + '" />'));
                    form.append($('<input type="hidden" name="backExists" value="' + (ShirtMaker3000.isBackCanvasEmpty() ? "false" : "true" ) + '" />'));
                    form.append($('<input type="hidden" name="' + ShirtMaker3000.side + "H" + '" value="' + (data[ShirtMaker3000.side].bottom) + '" />'));
                    form.append($('<input type="hidden" name="' + ShirtMaker3000.side + "W" + '" value="' + (data[ShirtMaker3000.side].right) + '" />'));
                    form.append($('<input type="hidden" name="' + otherSide + "H" + '" value="' + (data[otherSide].bottom) + '" />'));
                    form.append($('<input type="hidden" name="' + otherSide + "W" + '" value="' + (data[otherSide].right) + '" />'));
                    form.append($('<input type="hidden" name="' + otherSide + 'Size" value="' + ShirtMaker3000.printSizes[ShirtMaker3000.sideConfig[otherSide].printSize || 0].id + '" />'));
                    form.append($('<input type="hidden" name="' + ShirtMaker3000.side + 'Size" value="' + ShirtMaker3000.printSizes[ShirtMaker3000.filter.printSize || 0].id + '" />'));
                    form.append($('<input type="hidden" name="' + otherSide + '_os" value="' + ShirtMaker3000.printSizes[ShirtMaker3000.sideConfig[otherSide].printSize || 0].id + '" />'));
                    form.append($('<input type="hidden" name="' + ShirtMaker3000.side + '_os" value="' + ShirtMaker3000.printSizes[ShirtMaker3000.filter.printSize || 0].id + '" />'));
                    form.append($('<input type="hidden" name="variantid" value="' + ShirtMaker3000.selectedShirt.selectedVariant.id + '" />'));
                    form.append($('<input type="hidden" name="price" value="' + ShirtMaker3000.price + '" />'));
                    form.append($('<input type="hidden" name="size" value="' + ShirtMaker3000.filter.size + '" />'));
                    form.append($('<input type="hidden" name="overprint" value="' + (ShirtMaker3000.overprint ? 1 : 0) + '" />'));
                    form.append($('<textarea name="frontRecipe">' + frontRecipe + '</textarea>'));
                    form.append($('<textarea name="backRecipe">' + backRecipe + '</textarea>'));
                    form.append($('<textarea name="margins">' + JSON.stringify(data) + '</textarea>'));
                    form.submit();
                });
            } else {
                --canvasesToLoad;
            }
        }
    },

    generateSide: function (side) {
        ShirtMaker3000.loadShirtSpinner('show');

        var canvasFrontJsonStringify = JSON.stringify(ShirtMaker3000.variantConfig[side + 'Recipe']);
        var canvasFront = undefined;
        var scale = this.cmToPxMultiplayer / this.getWidth() * this.getRealWidth();

        var now = new Date().getTime();
        $('#invisibleTemplate').append('<canvas id="canvasTemplate' + side + '_' + now + '" width="' + (ShirtMaker3000.variantConfig[side].w * scale) + '" height="' + (ShirtMaker3000.variantConfig[side].h * scale) + '"></canvas>');

        if (ShirtMaker3000.variant == 4 && !isNaN(parseInt(ShirtMaker3000.variantConfig[side + 'ProductW'], 10)))
            scale = scale / parseInt(ShirtMaker3000.variantConfig[side + 'ProductW'], 10) * ShirtMaker3000.variantConfig[side].w;

        for (var idx in ShirtMaker3000.allowedImageFileTypes) {
            var el = ShirtMaker3000.allowedImageFileTypes[idx];
            var re = new RegExp('_m.' + el, 'g');
            canvasFrontJsonStringify = canvasFrontJsonStringify.replace(re, '.' + el);
        }

        canvasFrontJsonStringify = canvasFrontJsonStringify.replace(/http\:\/\/[\w\.]*\.blowart\.pl\/projects\//gi, 'http://blowart.pl/projects/');

        canvasFront = new fabric.Canvas('canvasTemplate' + side + '_' + now);

        canvasFront.loadFromJSON(canvasFrontJsonStringify, function () {
            try {
                var allObjects = canvasFront.getObjects();
                for (var idx in allObjects) {
                    var el = allObjects[idx];
                    el.setScaleX(el.get('scaleX') * scale);
                    el.setScaleY(el.get('scaleY') * scale);
                    el.left = el.left * scale;
                    el.top = el.top * scale;
                }

                canvasFront.renderAll();

                ShirtMaker3000.optimizeSideCanvas(side, canvasFront, canvasFront.width, canvasFront.height);
                canvasFront.setWidth(canvasFront.getWidth() * scale);
                canvasFront.setHeight(canvasFront.getHeight() * scale);
                canvasFront.renderAll();

                var image = canvasFront.toDataURL("image/png");
                ShirtMaker3000.loadShirtSpinner('hide');
                image = image.replace("image/png", "image/octet-stream");
                $('#invisibleTemplate').append('<a id="download_' + side + '_' + now + '" href="' + image + '" download="' + ShirtMaker3000.config.now + '_' + side + '.png"></a>');
                $('#download_' + side + '_' + now)[0].click();
            } catch (e) {
            }
        });
    },

    prepareFonts: function(){
        var now = new Date().getTime();
        $('#invisibleTemplate').append('<canvas id="canvasTemplateFonts_' + now + '" width="100" height="100"></canvas>');
        var canvasFont = new fabric.Canvas('canvasTemplateFonts_' + now);

        var fontsAll = JSON.parse(ShirtMaker3000.fonts);
        try{
            for(var i=0; i<fontsAll.length; ++i)
                canvasFont.add(new fabric.Text('', {fontFamily: fontsAll[i].name, left: 0, top: 0}) );
        }catch(e){}
    },

    start: function (id, variant, config) {
        var pid = undefined;

        ShirtMaker3000.variant = variant;
        ShirtMaker3000.variantConfig = config;

        if (config && config.loadSM) {
            ShirtMaker3000.loadSM = config.loadSM.split('_');
            if (ShirtMaker3000.loadSM.length != 3)
                ShirtMaker3000.loadSM = undefined;
        }

        if (variant == 4)
            ShirtMaker3000.editPositionCorrector = {top: 0, left: 0};

        Number.prototype.format = function (n, x) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
            return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
        };

        function start() {
            if (ShirtMaker3000.printSizes && pid && ShirtMaker3000.shirts && ShirtMaker3000.shirtsMultiplayers && ShirtMaker3000.printSizeInfo && ShirtMaker3000.fonts) {

                if(ShirtMaker3000.variantConfig && ShirtMaker3000.variantConfig.pid)
                    pid = ShirtMaker3000.variantConfig.pid;

                try {
                    ShirtMaker3000.create(id, pid);
                } catch (e) {
                }

                if (ShirtMaker3000.variant == 3 && ShirtMaker3000.variantConfig.overprint == '1') {
                    $(ShirtMaker3000.config.id + ' #gnr_opcje_produkt .gnr_grupa').addClass('permHide');
                    $(ShirtMaker3000.config.id + ' #gnr_opcje_produkt .gnr_produkty').addClass('permHide');
                    $(ShirtMaker3000.config.id + ' #gnr_opcje_produkt .gnr_grupa.rozmiar').removeClass('permHide');
                }

                if (config && config.front) {
                    var founded = undefined;
                    for (var i = 0; i < ShirtMaker3000.shirts.length; ++i) {
                        for (var j = 0; j < ShirtMaker3000.shirts[i].variants.length; ++j)
                            if (ShirtMaker3000.shirts[i].variants[j].id == config.id)
                                founded = j;
                        if (ShirtMaker3000.shirts[i].id == config.id || founded) {
                            ShirtMaker3000.selectShirt($(ShirtMaker3000.config.id + ' .gnr_produkty img[name=\'shirt_' + ShirtMaker3000.shirts[i].id + '\']')[0]);
                            if (founded)
                                ShirtMaker3000.selectColor($(ShirtMaker3000.config.id + ' #gnr_opcje_produkt .gnr_probnik_kolorow li:nth-child(' + (founded + 1) + ') a')[0]);
                            break;
                        }
                    }

                    var selectSiz = $(id + ' #gnr_opcje_produkt .selectSize li:contains("' + ShirtMaker3000.variantConfig.size + '")');
                    if(selectSiz.length)
                        ShirtMaker3000.selectSize(selectSiz[0]);
                    if (ShirtMaker3000.variant != 3 || ShirtMaker3000.variantConfig.overprint != '1')
                        $(id + ' .editBox').css({
                            top: ShirtMaker3000.variantConfig.front.y - ShirtMaker3000.selectedShirt.topFront,
                            left: ShirtMaker3000.variantConfig.front.x - ShirtMaker3000.selectedShirt.leftFront
                        });
                    if (ShirtMaker3000.variant == 4)
                        $(id + ' .editBox_2').css({
                            top: ShirtMaker3000.variantConfig.back.y - ShirtMaker3000.selectedShirt.topBack,
                            left: ShirtMaker3000.variantConfig.back.x - ShirtMaker3000.selectedShirt.leftBack
                        });
                }

                if(ShirtMaker3000.variant == 4)
                    ShirtMaker3000.prepareFonts();

                if (ShirtMaker3000.variant >= 3) {
                    $(ShirtMaker3000.config.id + ' .editBoxHandle').addClass('permHide');
                    if (ShirtMaker3000.variant == 3)
                        $(ShirtMaker3000.config.id + ' .editBox').css('cursor', 'move');

                    if (ShirtMaker3000.isFrontCanvasEmpty())
                        $('#gnr_ikonka_przod').css({display: 'none'});
                    if (ShirtMaker3000.isBackCanvasEmpty())
                        $('#gnr_ikonka_tyl').css({display: 'none'});
                }

                if (ShirtMaker3000.loadSM) {
                    var shirtSelect = $('#gnr_produkty_knt img[name=' + 'shirt_' + ShirtMaker3000.loadSM[0] + ']');
                    if (!shirtSelect.length)
                        shirtSelect = $('#gnr_produkty_knt img:nth-child(1)'); //select first
                    ShirtMaker3000.selectShirt(shirtSelect);

                    var colorSelect = false;
                    var colors = $('.gnr_probnik_kolorow li a');
                    for (var i = 0; i < colors.length; ++i) {
                        if ($(colors[i]).attr('color') == ShirtMaker3000.loadSM[1] || $(colors[i]).attr('color') == '#' + ShirtMaker3000.loadSM[1]) {
                            ShirtMaker3000.selectColor($(colors[i]));
                            colorSelect = true;
                        }
                    }
                    if(!colorSelect)
                        ShirtMaker3000.selectColor($(colors[0]));

                    var sizeButton = $('#gnr_rozmiary_opcje .gnr_roz_opcja.' + ShirtMaker3000.loadSM[2]);
                    if (!sizeButton.length)
                        sizeButton = $('#gnr_rozmiary_opcje .gnr_roz_opcja:nth-child(1)');
                    ShirtMaker3000.selectPrintSize(sizeButton, ShirtMaker3000.loadSM[2]);
                }

                if (ShirtMaker3000.variant == 2 && ShirtMaker3000.variantConfig && ShirtMaker3000.variantConfig.edit) {
                    var frontR = ShirtMaker3000.variantConfig.frontRecipe;
                    var backR = ShirtMaker3000.variantConfig.backRecipe;
                    if(ShirtMaker3000.variantConfig.margins)
                    {
                        if(frontR && frontR.objects && ShirtMaker3000.variantConfig.margins.front)
                            for(i=0; i<frontR.objects.length; ++i)
                            {
                                frontR.objects[i].left += ShirtMaker3000.variantConfig.margins.front.left;
                                frontR.objects[i].top += ShirtMaker3000.variantConfig.margins.front.top;
                            }
                        if(backR && backR.objects && ShirtMaker3000.variantConfig.margins.back)
                            for(i=0; i<backR.objects.length; ++i)
                            {
                                backR.objects[i].left += ShirtMaker3000.variantConfig.margins.back.left;
                                backR.objects[i].top += ShirtMaker3000.variantConfig.margins.back.top;
                            }
                    }

                    if (ShirtMaker3000.side == 'front') {
                        ShirtMaker3000.canvas.loadFromJSON(frontR, ShirtMakerVariantEditSetup);
                        ShirtMaker3000.sideConfig['back'] = ShirtMaker3000.sideConfig['back'] || {};
                        ShirtMaker3000.sideConfig['back'].canvas = backR;
                        ShirtMaker3000.sideConfig['back'].x = ShirtMaker3000.variantConfig['back'].x;
                        ShirtMaker3000.sideConfig['back'].y = ShirtMaker3000.variantConfig['back'].y;
                        ShirtMaker3000.sideConfig['back'].w = ShirtMaker3000.variantConfig['back'].w;
                        ShirtMaker3000.sideConfig['back'].h = ShirtMaker3000.variantConfig['back'].h;
                        for (var i = 0; i < ShirtMaker3000.printSizes.length; ++i)
                            if (ShirtMaker3000.printSizes[i].id == ShirtMaker3000.variantConfig['back'].s) {
                                ShirtMaker3000.sideConfig['back'].printSize = i;
                                break;
                            }
                    } else {
                        ShirtMaker3000.sideConfig['front'] = ShirtMaker3000.sideConfig['front'] || {};
                        ShirtMaker3000.sideConfig['front'].canvas = frontR;
                        ShirtMaker3000.sideConfig['front'].x = ShirtMaker3000.variantConfig['front'].x;
                        ShirtMaker3000.sideConfig['front'].y = ShirtMaker3000.variantConfig['front'].y;
                        ShirtMaker3000.sideConfig['front'].w = ShirtMaker3000.variantConfig['front'].w;
                        ShirtMaker3000.sideConfig['front'].h = ShirtMaker3000.variantConfig['front'].h;
                        for (var i = 0; i < ShirtMaker3000.printSizes.length; ++i)
                            if (ShirtMaker3000.printSizes[i].id == ShirtMaker3000.variantConfig['front'].s) {
                                ShirtMaker3000.sideConfig['front'].printSize = i;
                                break;
                            }
                        ShirtMaker3000.canvas.loadFromJSON(backR, ShirtMakerVariantEditSetup);
                    }
                } else
                    ShirtMaker3000.loadShirtSpinner('hide');

                function ShirtMakerVariantEditSetup(){
                    var colorSelect = false;
                    var colors = $('.gnr_probnik_kolorow li a');
                    for (var i = 0; i < colors.length; ++i) {
                        if ($(colors[i]).attr('color') == ShirtMaker3000.variantConfig['color'] || $(colors[i]).attr('color') == '#' + ShirtMaker3000.variantConfig['color']) {
                            ShirtMaker3000.selectColor($(colors[i]));
                            colorSelect = true;
                        }
                    }
                    if(!colorSelect)
                        ShirtMaker3000.selectColor($(colors[0]));

                    var sizeButton = $('#gnr_rozmiary_opcje .gnr_roz_opcja.' + ShirtMaker3000.variantConfig[ShirtMaker3000.side].s);
                    if (!sizeButton.length)
                        sizeButton = $('#gnr_rozmiary_opcje .gnr_roz_opcja:nth-child(1)');
                    ShirtMaker3000.selectPrintSize(sizeButton, ShirtMaker3000.variantConfig[ShirtMaker3000.side].s);

                    ShirtMaker3000.loadShirtSpinner('hide');
                    ShirtMaker3000.canvas.renderAll();
                }
            }
        }

        ShirtMaker3000.loadShirtSpinner('show');
        ShirtMaker3000.descriptionSpinner('show');

        $.get("/ShirtMaker3000.php?action=gpid", function (data) {
            pid = data;
            start();
        });
        $.get("/dane_do_generatora.php", function (data) {
            ShirtMaker3000.shirts = JSON.parse(data);
            start();
        });
        $.get("/dane_mnoznika_wielkosci_koszulki.php", function (data) {
            ShirtMaker3000.shirtsMultiplayers = JSON.parse(data) || {'S': 1.2, 'M': 1.1, 'L': 1, 'XL': 0.9, 'XXL': 0.8};
            start();
        });
        $.get("/dane_do_generatora-nadruk.php", function (data) {
            ShirtMaker3000.printSizes = JSON.parse(data);
            start();
        });
        $.get("/dane_ceny_nadrukow_id.php", function (data) {
            ShirtMaker3000.printSizeInfo = JSON.parse(data);
            start();
        });
    	
        $.ajax({
            url: "/dane_o_fontach.php",
            type: "GET",
            processData: false,
            contentType: false
        }).done(function (data) {
            ShirtMaker3000.fonts = data;
            start();
        });
    },

    create: function (id, now) {
        this.readConfig(id, now);
        $(id + ' #gnr_opcje_tekst .selectColor .choose').colorPicker({
            customBG: '#222',
            margin: '4px -2px 0',
            doRender: 'div div',

            buildCallback: function ($elm) {
                var colorInstance = this.color,
                    colorPicker = this;

                $elm.prepend('<div class="cp-panel">' +
                    'R <input type="text" class="cp-r" /><br>' +
                    'G <input type="text" class="cp-g" /><br>' +
                    'B <input type="text" class="cp-b" /><hr>' +
                    'H <input type="text" class="cp-h" /><br>' +
                    'S <input type="text" class="cp-s" /><br>' +
                    'B <input type="text" class="cp-v" /><hr>' +
                    '<input type="text" class="cp-HEX" />' +
                    '</div>').on('change', 'input', function (e) {
                    var value = this.value,
                        className = this.className,
                        type = className.split('-')[1],
                        color = {};

                    color[type] = value;
                    colorInstance.setColor(type === 'HEX' ? value : color,
                        type === 'HEX' ? 'HEX' : /(?:r|g|b)/.test(type) ? 'rgb' : 'hsv');
                    colorPicker.render();
                    this.blur();
                });
            },

            cssAddon: // could also be in a css file instead
            '.cp-color-picker{box-sizing:border-box; width:226px;}' +
            '.cp-color-picker .cp-panel {line-height: 21px; float:right;' +
            'padding:0 1px 0 8px; margin-top:-1px; overflow:visible}' +
            '.cp-xy-slider:active {cursor:none;}' +
            '.cp-panel, .cp-panel input {color:#bbb; font-family:monospace,' +
            '"Courier New",Courier,mono; font-size:12px; font-weight:bold;}' +
            '.cp-panel input {width:28px; height:12px; padding:2px 3px 1px;' +
            'text-align:right; line-height:12px; background:transparent;' +
            'border:1px solid; border-color:#222 #666 #666 #222;}' +
            '.cp-panel hr {margin:0 -2px 2px; height:1px; border:0;' +
            'background:#666; border-top:1px solid #222;}' +
            '.cp-panel .cp-HEX {width:44px; position:absolute; margin:1px -3px 0 -2px;}' +
            '.cp-alpha {width:155px;}',

            renderCallback: function ($elm, toggled) {
                var colors = this.color.colors.RND,
                    modes = {
                        r: colors.rgb.r, g: colors.rgb.g, b: colors.rgb.b,
                        h: colors.hsv.h, s: colors.hsv.s, v: colors.hsv.v,
                        HEX: this.color.colors.HEX
                    };

                $('input', '.cp-panel').each(function () {
                    this.value = modes[this.className.substr(3)];
                });

                if (ShirtMaker3000.textMenu.color.find('li.choose.active').length)
                    ShirtMaker3000.setTextColor('rgb(' + colors.rgb.r + ", " + colors.rgb.g + ", " + colors.rgb.b + ')');
            }
        });

        this.prepareCategories();

        $(function () {

            ShirtMaker3000.loadShirtMenu();

            $(id + ' .editMaxArea').css({
                width: ShirtMaker3000.config.maxAreaWidth,
                height: ShirtMaker3000.config.maxAreaHeight,
                top: parseInt(ShirtMaker3000.config.maxAreaTop, 10) + ShirtMaker3000.editPositionCorrector.top,
                left: parseInt(ShirtMaker3000.config.maxAreaLeft) + ShirtMaker3000.editPositionCorrector.left
            });
            if (ShirtMaker3000.variant == 4)
                $(id + ' .editMaxArea_2').css({
                    width: ShirtMaker3000.config.maxAreaWidth,
                    height: ShirtMaker3000.config.maxAreaHeight,
                    top: parseInt(ShirtMaker3000.config.maxAreaTop) + ShirtMaker3000.editPositionCorrector.top,
                    left: parseInt(ShirtMaker3000.config.maxAreaLeft) + ShirtMaker3000.editPositionCorrector.left
                });

            if (ShirtMaker3000.variant >= 3) {
                $(id + ' .editBox').html('<img class="variant3Image" src="' + ShirtMaker3000.variantConfig.front.project + '" id="ShirtMaker3000_' + now + '" width="' + ShirtMaker3000.variantConfig.front.w + '" height="' + ShirtMaker3000.variantConfig.front.h + '" />');
                $(id + ' .editBox').css({width: ShirtMaker3000.variantConfig.front.w, height: ShirtMaker3000.variantConfig.front.h, border: 'none'});

                if (ShirtMaker3000.variantConfig.overprint == '1') {
                    $(id + ' .editMaxArea').css({width: '100%', height: '100%', top: 0, left: 0});
                    $(id + ' .editMaxArea').addClass('overprint');
                    $(id + ' .editMaxArea .variant3Image').css({'cursor': 'default'});
                }

                if (!ShirtMaker3000.variantConfig.front) $(id + ' #gnr_ikonka_przod').css({visibility: 'hidden'});
                if (!ShirtMaker3000.variantConfig.back) $(id + ' #gnr_ikonka_tyl').css({visibility: 'hidden'});

                if (ShirtMaker3000.variant == 4) {
                    if (!ShirtMaker3000.isBackCanvasEmpty()) {
                        $(id + ' .editBox_2').html('<img class="variant3Image" style="border: none;" src="' + ShirtMaker3000.variantConfig.back.project + '" width="' + ShirtMaker3000.variantConfig.back.w + '" height="' + ShirtMaker3000.variantConfig.back.h + '" />');
                        $(id + ' .editBox_2').css({width: ShirtMaker3000.variantConfig.back.w, height: ShirtMaker3000.variantConfig.back.h, border: 'none'});
                    } else {
                        $(id + ' #gnr_rozmiar_2').css({display: 'none'});
                        $(id + ' #gnr_ikonka_przod_2').css({display: 'none'});
                    }
                }
            }
            else {
                $(id + ' .editBox').html('<div class="editBoxHandle glyphicon glyphicon-move"></div><canvas id="ShirtMaker3000_' + now + '" width="' + ShirtMaker3000.config.editBoxWidth + '" height="' + ShirtMaker3000.config.editBoxHeight + '"></canvas>');
                $(id + ' .editBox').css({
                    width: ShirtMaker3000.config.editBoxWidth,
                    height: ShirtMaker3000.config.editBoxHeight,
                    top: ShirtMaker3000.config.editBoxTop,
                    left: ShirtMaker3000.config.editBoxLeft
                });

                ShirtMaker3000.canvas = new fabric.Canvas('ShirtMaker3000_' + now);
                ShirtMaker3000.canvas.controlsAboveOverlay = true;

                //get controls refs
                ShirtMaker3000.textMenu.color = $(id + ' .textMenu .selectColor');
                ShirtMaker3000.textMenu.textDecoration = $(id + ' .textMenu .underline');
                ShirtMaker3000.textMenu.fontStyle = $(id + ' .textMenu .italic');
                ShirtMaker3000.textMenu.fontWeight = $(id + ' .textMenu .bold');
                ShirtMaker3000.textMenu.fontFamily = $(id + ' .textMenu .fontFamily');
                ShirtMaker3000.textMenu.fontSize = $(id + ' .textMenu .fontSize');
                ShirtMaker3000.textMenu.val = $(id + ' .textMenu textarea');
                ShirtMaker3000.textMenu.align = $(id + ' .textMenu .align');

                ShirtMaker3000.textMenu.val.css({fontFamily: ShirtMaker3000.textMenu.fontFamily.val()});

                //load fonts
                ShirtMaker3000.textMenu.fontFamily.find('option').each(function (e) {
                    var text = new fabric.Text('', {fontFamily: $(this).val(), left: -100, top: -100});
                    ShirtMaker3000.canvas.add(text);
                });

                //actions
                //textarea
                ShirtMaker3000.textMenu.val.keyup(function () {
                    var el = ShirtMaker3000.canvas.getActiveObject();
                    if (el && el.type === 'text') {
                        el.set({text: ShirtMaker3000.textMenu.val.val()});
                        ShirtMaker3000.canvas.renderAll();
                    }
                });
                //fontFamily
                ShirtMaker3000.textMenu.fontFamily.change(function () {
                    var el = ShirtMaker3000.canvas.getActiveObject();
                    if (el && el.type === 'text') {
                        el.set({fontFamily: ShirtMaker3000.textMenu.fontFamily.val()});
                        ShirtMaker3000.canvas.renderAll();
                    }
                    ShirtMaker3000.textMenu.val.css({fontFamily: ShirtMaker3000.textMenu.fontFamily.val()});
                });
                //fontSize
                ShirtMaker3000.textMenu.fontSize.change(function () {
                    var el = ShirtMaker3000.canvas.getActiveObject();
                    if (el && el.type === 'text') {
                        el.setFontSize(ShirtMaker3000.textMenu.fontSize.val());
                        ShirtMaker3000.canvas.renderAll();
                    }
                });

                $(id + ' #gnr_opcje_menu div').each(function (idx) {
                    $(this).click(function () {
                        $(id + ' #gnr_opcje_menu div').each(function (idx) {
                            $(this).removeClass('active');
                        });
                        $(id + ' .menuBox').each(function (idx) {
                            $(this).removeClass('active');
                        });
                        $(this).addClass('active');

                        var menuBox = '#gnr_opcje_produkt';
                        switch (idx) {
                            case 1:
                                menuBox = '.fileMenu';
                                break;
                            case 2:
                                menuBox = '.textMenu';
                                break;
                        }

                        $(id + ' .menu ' + menuBox).addClass('active');
                    });
                });
                $(id + ' .textMenu .options .kind div').each(function (idx) {
                    $(this).click(function () {
                        var el = $(this);
                        if (el.hasClass('active'))
                            el.removeClass('active');
                        else {
                            el.addClass('active');
                        }

                        ShirtMaker3000.textMenu.val.css({
                            textDecoration: ShirtMaker3000.textMenu.textDecoration.hasClass('active') ? 'underline' : '',
                            fontStyle: ShirtMaker3000.textMenu.fontStyle.hasClass('active') ? 'italic' : '',
                            fontWeight: ShirtMaker3000.textMenu.fontWeight.hasClass('active') ? 'bold' : '',
                            textAlign: ShirtMaker3000.textMenu.align.hasClass('active') ? 'bold' : ''
                        });

                        el = ShirtMaker3000.canvas.getActiveObject();
                        if (el && el.type === 'text') {
                            el.set({
                                textDecoration: ShirtMaker3000.textMenu.textDecoration.hasClass('active') ? 'underline' : '',
                                fontStyle: ShirtMaker3000.textMenu.fontStyle.hasClass('active') ? 'italic' : '',
                                fontWeight: ShirtMaker3000.textMenu.fontWeight.hasClass('active') ? 'bold' : ''
                            });
                            ShirtMaker3000.canvas.renderAll();
                        }
                    });
                });
                ShirtMaker3000.textMenu.color.find('li').each(function (idx) {
                    $(this).click(function () {
                        ShirtMaker3000.textMenu.color.find('li').each(function () {
                            $(this).removeClass('active');
                        });
                        $(this).addClass('active');

                        ShirtMaker3000.setTextColor($(this).css('background-color'));
                    });
                });
                ShirtMaker3000.textMenu.align.find('.left, .right, .center').each(function (idx) {
                    $(this).click(function () {
                        ShirtMaker3000.textMenu.align.find('.left, .right, .center').each(function () {
                            $(this).removeClass('active');
                        });
                        $(this).addClass('active');

                        var el = ShirtMaker3000.canvas.getActiveObject();
                        if (el && el.type === 'text') {
                            el.set({textAlign: $(this).attr('class').replace(/[ ]?active/gi, '')});
                            ShirtMaker3000.canvas.renderAll();
                        }
                    });
                });

                $(id + ' .textMenu .addText').click(function () {
                    if (!ShirtMaker3000.textMenu.val.val())
                        return;

                    var text = new fabric.Text(ShirtMaker3000.textMenu.val.val(), ShirtMaker3000.controlls({
                        fontFamily: ShirtMaker3000.textMenu.fontFamily.val(),
                        fill: ShirtMaker3000.textMenu.color.find('li.active').css('background-color') || '#000',
                        fontSize: ShirtMaker3000.textMenu.fontSize.val(),
                        textDecoration: ShirtMaker3000.textMenu.textDecoration.hasClass('active') ? 'underline' : '',
                        fontStyle: ShirtMaker3000.textMenu.fontStyle.hasClass('active') ? 'italic' : '',
                        fontWeight: ShirtMaker3000.textMenu.fontWeight.hasClass('active') ? 'bold' : '',
                        left: 0,
                        top: 0
                    }));
                    if (ShirtMaker3000.textMenu.align.find('.active').length)
                        text.textAlign = ShirtMaker3000.textMenu.align.find('.active').attr('class').replace(/[ ]?active/gi, '');
                    if (text.width > parseInt(ShirtMaker3000.config.editBoxWidth, 10)) {
                        text.scale(parseInt(ShirtMaker3000.config.editBoxWidth, 10) / text.width);
                    }
                    text.left = (parseInt(ShirtMaker3000.config.editBoxWidth, 10) - text.width) / 2;
                    text.top = (parseInt(ShirtMaker3000.config.editBoxHeight, 10) - text.height) / 2;
                    if (text.left < 0) text.left = 0;
                    if (text.top < 0) text.top = 0;
                    ShirtMaker3000.canvas.add(text);
                    ShirtMaker3000.overprintStayAtTop();
                    ShirtMaker3000.overprintEditBoxPosition();
                    ShirtMaker3000.canvas.setActiveObject(text);
                    ShirtMaker3000.canvas.renderAll();
                    ShirtMaker3000.recountPrice();
                });

                ShirtMaker3000.canvas.observe('object:selected', function (e) {
                    if (e.target.type === 'text')
                        ShirtMaker3000.updateTextControls(e);
                });

                ShirtMaker3000.canvas.observe('object:remove', function (e) {
                    ShirtMaker3000.recountPrice();
                });

                $(id + ' .fileMenu #zaladuj_plik_btn').click(ShirtMaker3000.makeSureRights);
                $(id + ' #rightsAlert .gnr_button').click(function () {
                    $(id + ' .fileMenu input[name="upload"]').click();
                });
                $(id + ' #rightsAlert .gnr_button_anuluj').click(function () {
                    $('#rightsAlert').dialog('close');
                });
                $(id + ' .fileMenu input[name="upload"]').change(ShirtMaker3000.loadImage);

                //history
                ShirtMaker3000.canvas.on('object:added', function (e) {
                    var object = e.target;
                    if (ShirtMaker3000.history.action === true) {
                        ShirtMaker3000.history.state = [ShirtMaker3000.history.state[ShirtMaker3000.history.index2]];
                        ShirtMaker3000.history.list = [ShirtMaker3000.history.list[ShirtMaker3000.history.index2]];
                        ShirtMaker3000.history.action = false;
                        ShirtMaker3000.history.index = 1;
                    }
                    object.saveState(); //save object
                    ShirtMaker3000.history.state[ShirtMaker3000.history.index] = JSON.stringify(object.originalState);
                    ShirtMaker3000.history.list[ShirtMaker3000.history.index] = object;
                    ShirtMaker3000.history.index++;
                    ShirtMaker3000.history.index2 = ShirtMaker3000.history.index - 1;
                    ShirtMaker3000.history.refresh = true;
                });

                ShirtMaker3000.canvas.on('object:modified', function (e) {
                    var object = e.target;
                    if (ShirtMaker3000.history.action === true) {
                        ShirtMaker3000.history.state = [ShirtMaker3000.history.state[ShirtMaker3000.history.index2]];
                        ShirtMaker3000.history.list = [ShirtMaker3000.history.list[ShirtMaker3000.history.index2]];
                        ShirtMaker3000.history.action = false;
                        ShirtMaker3000.history.index = 1;
                    }
                    object.saveState();
                    ShirtMaker3000.history.state[ShirtMaker3000.history.index] = JSON.stringify(object.originalState);
                    ShirtMaker3000.history.list[ShirtMaker3000.history.index] = object;
                    ShirtMaker3000.history.index++;
                    ShirtMaker3000.history.index2 = ShirtMaker3000.history.index - 1;
                    ShirtMaker3000.history.refresh = true;
                });

                ShirtMaker3000.undo = function () {

                    if (ShirtMaker3000.history.index <= 0) {
                        ShirtMaker3000.history.index = 0;
                        return;
                    }

                    if (ShirtMaker3000.history.refresh === true) {
                        ShirtMaker3000.history.index--;
                        ShirtMaker3000.history.refresh = false;
                    }

                    ShirtMaker3000.history.index2 = ShirtMaker3000.history.index - 1;
                    ShirtMaker3000.history.current = ShirtMaker3000.history.list[ShirtMaker3000.history.index2];
                    ShirtMaker3000.history.current.setOptions(JSON.parse(ShirtMaker3000.history.state[ShirtMaker3000.history.index2]));
                    ShirtMaker3000.history.index--;
                    ShirtMaker3000.history.current.setCoords();
                    ShirtMaker3000.canvas.renderAll();
                    ShirtMaker3000.history.action = true;
                }

                ShirtMaker3000.redo = function () {
                    ShirtMaker3000.history.action = true;
                    if (ShirtMaker3000.history.index >= ShirtMaker3000.history.state.length - 1) {
                        return;
                    }

                    ShirtMaker3000.history.index2 = ShirtMaker3000.history.index + 1;
                    ShirtMaker3000.history.current = ShirtMaker3000.history.list[ShirtMaker3000.history.index2];
                    ShirtMaker3000.history.current.setOptions(JSON.parse(ShirtMaker3000.history.state[ShirtMaker3000.history.index2]));
                    ShirtMaker3000.history.index++;
                    ShirtMaker3000.history.current.setCoords();
                    ShirtMaker3000.canvas.renderAll();
                }

                ShirtMaker3000.restart = function () {
                    ShirtMaker3000.history.list = [];
                    ShirtMaker3000.history.state = [];
                    ShirtMaker3000.history.index = 0;
                    ShirtMaker3000.history.index2 = 0;
                    ShirtMaker3000.history.action = false;
                    ShirtMaker3000.history.refresh = true;
                    ShirtMaker3000.canvas.clear().renderAll();


                    if (ShirtMaker3000.overprint && ShirtMaker3000.overprint.frontImg && ShirtMaker3000.side == 'front') {
                        ShirtMaker3000.canvas.add(ShirtMaker3000.overprint.frontImg);
                        ShirtMaker3000.canvas.bringToFront(ShirtMaker3000.overprint.frontImg);
                        ShirtMaker3000.canvas.renderAll();
                    }
                    if (ShirtMaker3000.overprint && ShirtMaker3000.overprint.backImg && ShirtMaker3000.side == 'back') {
                        ShirtMaker3000.canvas.add(ShirtMaker3000.overprint.backImg);
                        ShirtMaker3000.canvas.bringToFront(ShirtMaker3000.overprint.backImg);
                        ShirtMaker3000.canvas.renderAll();
                    }
                    this.overprintEditBoxPosition();
                }
            }

            var draggableConf = {
                containment: id + " .editMaxArea",
                scroll: false,
                cancel: id + " .canvas-container"
            };
            if (ShirtMaker3000.variant <= 2)
                draggableConf.handle = ".editBoxHandle";
            if (ShirtMaker3000.variant <= 3 && (!ShirtMaker3000.variantConfig || ShirtMaker3000.variantConfig.overprint != '1'))
                $(id + " .editBox").draggable(draggableConf);

            if (ShirtMaker3000.variant == 3 && (!ShirtMaker3000.variantConfig || ShirtMaker3000.variantConfig.overprint != '1')) {
                $(id + " .editBox").resizable({
                    handles: 'all',
                    aspectRatio: true,
                    alsoResize: id + " .editBox img",
                    stop: function () {
                        var editBox = $(id + " .editBox");
                        var outerBox = $(id + ' .editMaxArea');
                        if (editBox.position().left + editBox.width() > outerBox.width()) editBox.css({left: outerBox.width() - editBox.width()});
                        if (editBox.position().top + editBox.height() > outerBox.height()) editBox.css({top: outerBox.height() - editBox.height()});

                        ShirtMaker3000.recountPrice();
                    }
                });
            }

            ShirtMaker3000.resizeEditor();
            try {
                $(".nano").nanoScroller();
            } catch (e) {
            }
        });
    },

    showEditOptions: function(){
        $(ShirtMaker3000.config.id + ' #gnr_opcje_wrapper').toggleClass("collapsed");
    },

    optimizeSideCanvas: function (side, canvasSide, width, height) {
        var margins = {};
        try {
            var allObjects = canvasSide.getObjects();
            for (var idx in allObjects) {
                var el = allObjects[idx];
                var pom = el.getBoundingRect();
                if (el.type == 'text' && !el.text)
                    continue;
                if (el.type == 'text') {
                    var marg = 10;
                    pom.left -= marg;
                    pom.top -= marg;
                    pom.width += 2 * marg;
                    pom.height += 2 * marg;
                }
                if (el.type == 'image') {
                    ShirtMaker3000.applyFilterOnImage(el, canvasSide);
                }
                if (margins.left == undefined || pom.left < margins.left)
                    margins.left = pom.left;
                if (margins.top == undefined || pom.top < margins.top)
                    margins.top = pom.top;
                if (margins.right == undefined || (pom.width + pom.left) > margins.right)
                    margins.right = pom.width + pom.left;
                if (margins.bottom == undefined || (pom.height + pom.top) > margins.bottom)
                    margins.bottom = pom.height + pom.top;
            }

            if (!margins.left || margins.left < 0) margins.left = 0;
            if (!margins.top || margins.top < 0) margins.top = 0;
            if (margins.right > width) margins.right = width;
            if (margins.bottom > height) margins.bottom = height;
            margins.right = margins.right - margins.left || 0;
            margins.bottom = margins.bottom - margins.top || 0;

            //resize
            for (var idx in allObjects) {
                var el = allObjects[idx];
                el.left -= margins.left;
                el.top -= margins.top;
            }
            canvasSide.setWidth(margins.right);
            canvasSide.setHeight(margins.bottom);
            canvasSide.renderAll();
            return margins;
        } catch (e) {
        }
        return false;
    },

    getSideCanvasJson: function (side) {
        return JSON.stringify(this.getSideCanvas(side));
    },

    getSideCanvas: function (side) {
        return this.side == side ? this.canvas : (this.sideConfig[side] ? this.sideConfig[side].canvas : undefined);
    },

    getSidePrintSize: function (side) {
        if (this.variant >= 3) {
            var box = $(this.config.id + ' .editBox');
            var width = (this.side == side ? box.width() : (this.sideConfig[side] || this.variantConfig[side]).w);
            var height = (this.side == side ? box.height() : (this.sideConfig[side] || this.variantConfig[side]).h);
            var realWidth = width / (this.getWidth(side) / this.getRealWidth(side)) / this.shirtsMultiplayers[this.filter.size];
            var realHeight = height / (this.getHeight(side) / this.getRealHeight(side)) / this.shirtsMultiplayers[this.filter.size];

            if (ShirtMaker3000.variant >= 3 && ShirtMaker3000.variantConfig.overprint == '1') {
                for (var x = 0; x < ShirtMaker3000.selectedShirt.printSizeId.length; ++x) {
                    for (var y = 0; y < ShirtMaker3000.printSizeInfo.length; ++y) {
                        if (ShirtMaker3000.selectedShirt.printSizeId[x] == ShirtMaker3000.printSizeInfo[y].id && ShirtMaker3000.printSizeInfo[y].overprint == '1') {
                            $(ShirtMaker3000.config.id + ' #gnr_rozmiar_nadruku').html('<div class="gnr_roz_opcja"><strong>' + ShirtMaker3000.printSizeInfo[y].wielkosc + '</strong><br>' +
                                ShirtMaker3000.printSizeInfo[y].width + 'x' + ShirtMaker3000.printSizeInfo[y].height + '</div>');

                            for (var j = 0; j < ShirtMaker3000.printSizes.length; ++j)
                                if (ShirtMaker3000.printSizes[j].id == ShirtMaker3000.printSizeInfo[y].id)
                                    return j;
                        }
                    }
                }
            } else
                for (var i = 0; i < ShirtMaker3000.printSizes.length; ++i) {
                    if (i == 2 || realWidth < parseInt(ShirtMaker3000.printSizes[i].width, 10) && realHeight < parseInt(ShirtMaker3000.printSizes[i].height, 10)) {
                        if (this.side == side)
                            $(ShirtMaker3000.config.id + ' #gnr_rozmiar_nadruku').html('<div class="gnr_roz_opcja"><strong>' + ShirtMaker3000.printSizes[i].name + '</strong><br>' +
                                ShirtMaker3000.printSizes[i].width + 'x' + ShirtMaker3000.printSizes[i].height + '</div>');
                        ShirtMaker3000[side + 'PrinterSize'] = ShirtMaker3000.printSizes[i].id;
                        return i;
                    }
                }
        }
        return this.side == side ? this.filter.printSize : (this.sideConfig[side] ? this.sideConfig[side].printSize : undefined);
    },

    isFrontCanvasEmpty: function () {
        if (ShirtMaker3000.variant >= 3)
            return !ShirtMaker3000.variantConfig.front.project;

        if (!ShirtMaker3000.textMenu.fontFamily)
            return true;
        var canv = this.getSideCanvas('front');
        if (canv)
            return (canv.objects || canv.getObjects()).length <= ShirtMaker3000.textMenu.fontFamily.find('option').length;
        else
            return true;
    },

    isBackCanvasEmpty: function () {
        if (ShirtMaker3000.variant >= 3)
            return !ShirtMaker3000.variantConfig.back.project;

        var canv = this.getSideCanvas('back');
        if (canv)
            return (canv.objects || canv.getObjects()).length == 0;
        else
            return true;
    },

    isCanvasEmpty: function (side) {
        if (side == 'front') return this.isFrontCanvasEmpty();
        else if (side == 'back') return this.isBackCanvasEmpty();
        else return undefined;
    },

    descriptionSpinner: function (action) {
        if (action == 'show') {
            $('.descriptionSpinner').addClass('active');
            $('.descriptionSpinnerRefresh').addClass('active');
            $('.descriptionSpinnerText').addClass('active');
        } else {
            $('.descriptionSpinner').removeClass('active');
            $('.descriptionSpinnerRefresh').removeClass('active');
            $('.descriptionSpinnerText').removeClass('active');
        }
    },

    uploadSpinner: function (action) {
        if (action == 'show') {
            $('.uploadFileSpinner').addClass('active');
            $('.uploadFileSpinnerRefresh').addClass('active');
            $('.uploadFileSpinnerText').addClass('active');
        } else {
            $('.uploadFileSpinner').removeClass('active');
            $('.uploadFileSpinnerRefresh').removeClass('active');
            $('.uploadFileSpinnerText').removeClass('active');
        }
    },

    loadShirtSpinner: function (action) {
        if (action == 'show') {
            $('.loadShirtSpinner').addClass('active');
            $('.loadShirtSpinnerRefresh').addClass('active');
            $('.loadShirtSpinnerText').addClass('active');
        } else {
            $('.loadShirtSpinner').removeClass('active');
            $('.loadShirtSpinnerRefresh').removeClass('active');
            $('.loadShirtSpinnerText').removeClass('active');
        }
    },

    loadDescIdx: 0,
    loadDescription: function (loadDescIdx) {
        if (ShirtMaker3000.variant == 4) return;
        this.descriptionSpinner('show');
        $.get("/dane_koszulka.php?id=" + this.selectedShirt.selectedVariant.id, function (data) {
            ShirtMaker3000.descriptionSpinner('hide');
            if (loadDescIdx == ShirtMaker3000.loadDescIdx)
                $('#koszulka_opis .body').html(data);
        });
    },

    clear: function () {
        this.canvas.clear().renderAll();
    },

    makeSureRights: function () {
        $('#rightsAlert').dialog({position: {my: "center", at: "center", of: ShirtMaker3000.config.id + ' .editArea'}});
    },

    loadImage: function (e) {
        $('#rightsAlert').dialog('close');
        ShirtMaker3000.uploadSpinner('show');
        var file = e.target.files[0];
        if (!file.type.match('image.*')) {
            alert('Wybierz plik do przesłania. Maksymalny dopuszczalny rozmiar to 10MB, obsługiwane formaty to JPG, PNG, GIF.');
            ShirtMaker3000.uploadSpinner('hide');
            return;
        }

        var fd = new FormData();
        fd.append('upload', $('#zaladuj_plik_file')[0].files[0]);
        fd.append("action", "uftp");
        fd.append("pid", ShirtMaker3000.config.now);
        $.ajax({
            url: "/ShirtMaker3000.php",
            type: "POST",
            data: fd,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false
        }).done(function (data) {
            ShirtMaker3000.uploadSpinner('hide');
            if (data.indexOf('error:') == 0) {
                alert(data.substring(7));
            } else if (!data) {
                alert("Plik jest zbyt du\u017cy, maksymalny dopuszczalny rozmiar to 10MB.");
            } else {
                var info = JSON.parse(data);
                $(ShirtMaker3000.config.id + ' .fileMenu .gnr_grafiki').append('<div onclick="ShirtMaker3000.addImageToCanvas(this, ' + info.w + ', ' + info.h + ', \'' + info.url + '\');" class="thumbnail" style="background-image: url(\'' + info.url + '\');" />');
            }
        });
        return false;
    },

    addImageToCanvas: function (el, w, h, src) {
        var imgElement = $("<img src=\"" + src + "\" />")[0];
        var width = this.filter.scale * parseInt(w, 10) / this.cmToPxMultiplayer;
        var height = this.filter.scale * parseInt(h, 10) / this.cmToPxMultiplayer;

        var propW = 0;
        var propH = 0;
        if (width > ShirtMaker3000.config.editBoxWidth) propW = width / ShirtMaker3000.config.editBoxWidth;
        if (height > ShirtMaker3000.config.editBoxHeight) propH = height / ShirtMaker3000.config.editBoxHeight;
        var prop = Math.max(propH, propW);
        if (prop) {
            width = width / prop;
            height = height / prop;
        }

        var left = (parseInt(ShirtMaker3000.config.editBoxWidth, 10) - width) / 2;
        var top = (parseInt(ShirtMaker3000.config.editBoxHeight, 10) - height) / 2;
        var imgInstance = new fabric.Image(imgElement, ShirtMaker3000.controlls({left: left, top: top, height: height, width: width}));
        this.canvas.add(imgInstance);
        ShirtMaker3000.overprintStayAtTop();
        this.overprintEditBoxPosition();

        ShirtMaker3000.applyFilterOnImage(imgInstance, ShirtMaker3000.canvas);

        ShirtMaker3000.canvas.renderAll();
        ShirtMaker3000.recountPrice();
    },

    applyFilterOnImage: function (img, canvas) {
        var filter = new fabric.Image.filters.Convolute({
            matrix: [1 / 9, 1 / 9, 1 / 9,
                1 / 9, 1 / 9, 1 / 9,
                1 / 9, 1 / 9, 1 / 9]
        });
        img.filters.push(filter);
        img.applyFilters(canvas.renderAll.bind(canvas));
    },

    updateTextControls: function (e) {
        this.textMenu.val.val(e.target.get('text'));
        if (e.target.get('textDecoration') && e.target.get('textDecoration') != 'none')
            this.textMenu.textDecoration.addClass('active');
        else
            this.textMenu.textDecoration.removeClass('active');
        if (e.target.get('fontStyle') && e.target.get('fontStyle') != 'none')
            this.textMenu.fontStyle.addClass('active');
        else
            this.textMenu.fontStyle.removeClass('active');
        if (e.target.get('fontWeight') && e.target.get('fontWeight') != 'none')
            this.textMenu.fontWeight.addClass('active');
        else
            this.textMenu.fontWeight.removeClass('active');
        this.textMenu.fontFamily.val(e.target.get('fontFamily'));
        this.textMenu.fontSize.val(e.target.get('fontSize'));
        ShirtMaker3000.textMenu.color.find('li').each(function () {
            $(this).removeClass('active');
        });
        this.textMenu.color.find('li').each(function () {
            if ($(this).css('background-color') == ShirtMaker3000.hexToRgb(e.target.get('fill'))) $(this).addClass('active');
        });
        ShirtMaker3000.textMenu.align.find('.left, .right, .center').each(function () {
            $(this).removeClass('active');
        });
        this.textMenu.align.find('.left, .right, .center').each(function () {
            if ($(this).attr('class') == e.target.get('textAlign')) $(this).addClass('active');
        });
    },

    readConfig: function (id, now) {
        this.config = this.shirtMaker3000DefaultConfig;
        this.config.id = id;
        this.config.now = now;
        var configFielfs = Object.keys(this.shirtMaker3000DefaultConfig);
        for (var idx in configFielfs)
            this.config[configFielfs[idx]] = this.config[configFielfs[idx]] || this.shirtMaker3000DefaultConfig[configFielfs[idx]];
    },

    controlls: function (conf) {
        for (var idx in this.defaultControlls)
            conf[idx] = conf[idx] || this.defaultControlls[idx];
        return conf;
    },

    hexToRgb: function (hex) {
        if (hex.indexOf('#') != 0) return hex;
        var bigint = parseInt(hex, 16);
        var r = (bigint >> 16) & 255;
        var g = (bigint >> 8) & 255;
        var b = bigint & 255;

        return 'rgb(' + r + ", " + g + ", " + b + ')';
    },

    loadShirtMenu: function () {
        for (var i = 0; i < this.forWhom.length; ++i)
            $(this.config.id + ' #gnr_opcje_produkt .forWhom .gnr_opcje').append('<li onclick="ShirtMaker3000.selectWhom(this);" class="dlaKogo' + i + '">' + this.forWhom[i] + '</li>');
        $(this.config.id + ' #gnr_opcje_produkt .forWhom .gnr_opcje').append('<li onclick="ShirtMaker3000.selectWhom(this);" class="dlaKogoAll selected">Wszystko</li>');

        this.loadShirtKind();
    },

    loadShirtKind: function () {
        $(this.config.id + ' #gnr_opcje_produkt .selectKind').html('');
        for (var i = 0; i < this.shirtKind.length; ++i) {
            $(this.config.id + ' #gnr_opcje_produkt .selectKind').append('<li onclick="ShirtMaker3000.selectKind(this);" class="rodzaj' + i + '">' + this.shirtKind[i] + '</li>');
        }
        $(this.config.id + ' #gnr_opcje_produkt .selectKind').append('<li onclick="ShirtMaker3000.selectKind(this);" class="rodzajAll selected">Wszystko</li>');

        this.loadShirtType();
    },

    loadShirtType: function () {
        $(this.config.id + ' #gnr_opcje_produkt .selectQuality .quality').css({display: this.quality ? 'inline-block' : 'none'});
        $(this.config.id + ' #gnr_opcje_produkt .selectQuality .economy').css({display: this.economy ? 'inline-block' : 'none'});
        this.selectQuality($(this.config.id + ' #gnr_opcje_produkt .selectQuality .all')[0], 'wszystko');
    },

    loadShirts: function () {
        $(this.config.id + ' #gnr_opcje_produkt .gnr_produkty').html('');
        var shirts = this.getFilteredShirts();
        for (var i = 0; i < shirts.length; ++i)
            $(this.config.id + ' #gnr_opcje_produkt .gnr_produkty').append('<img onclick="ShirtMaker3000.selectShirt(this);" name="shirt_' + shirts[i].id + '" src="' + shirts[i].variants[0][this.side] + '" class="' + (i == 0 ? 'selected' : '') + '" />');

        this.selectShirt($(this.config.id + ' #gnr_opcje_produkt .gnr_produkty img:nth-child(1)'));
        this.loadShirtOptions();
    },

    loadShirtOptions: function () {
        $(this.config.id + ' #gnr_opcje_produkt .selectSize').html('');
        for (var i = 0; i < this.sizes.length; ++i)
            $(this.config.id + ' #gnr_opcje_produkt .selectSize').append('<li onclick="ShirtMaker3000.selectSize(this);" class="' + (i == 0 ? 'selected' : '') + '">' + this.sizes[i] + '</li>');
        this.filter.size = this.sizes[0];

        $(this.config.id + ' #gnr_opcje_produkt .gnr_probnik_kolorow').html('');
        for (var i = 0; i < this.colors.length; ++i)
            $(this.config.id + ' #gnr_opcje_produkt .gnr_probnik_kolorow').append('<li><a onclick="ShirtMaker3000.selectColor(this);" href="javascript:void(0)" style="background-color: ' + this.colors[i] + ';" color="' + this.colors[i] + '" class="' + (i == 0 ? 'selected' : '') + '">&nbsp;</a></li>');
        this.filter.color = this.colors[0];
    },

    selectWhom: function (el) {
        $(this.config.id + ' #gnr_opcje_produkt .forWhom .gnr_opcje li').each(function () {
            $(this).removeClass('selected');
        });
        $(el).addClass('selected');

        this.setFilterWhom($(el).html());
        this.recountPrice();
    },

    selectSide: function (side, el) {
        if (this.variant < 3)
            ShirtMaker3000.loadShirtSpinner('show');

        var box = $(this.config.id + ' .editBox');

        if (this.overprint && this.overprint.frontImg && this.side == 'front') {
            this.canvas.remove(this.overprint.frontImg);
        }
        if (this.overprint && this.overprint.backImg && this.side == 'back') {
            this.canvas.remove(this.overprint.backImg);
        }
        this.overprintEditBoxPosition();

        var emptyHistory = true;
        if (this.variant >= 3) {
            //backup
            this.sideConfig[this.side] = this.sideConfig[this.side] || {};
            this.sideConfig[this.side].x = box.position().left;
            this.sideConfig[this.side].y = box.position().top;
            this.sideConfig[this.side].w = box.width();
            this.sideConfig[this.side].h = box.height();
            this.sideConfig[this.side].printSize = this.filter.printSize;

            this.side = side;
            this.sideConfig[this.side] = this.sideConfig[this.side] || {};

            //restore
            this.sideConfig[this.side] = this.sideConfig[this.side] || {};
            this.filter.printSize = (this.sideConfig[this.side] || {}).printSize !== undefined ? this.sideConfig[this.side].printSize : this.sideConfig['front'].printSize;
            box.css({
                left: this.sideConfig[this.side].x !== undefined ? this.sideConfig[this.side].x : this.variantConfig[this.side].x - ShirtMaker3000.selectedShirt['left' + (this.side == 'front' ? 'Front' : 'Back')],
                top: this.sideConfig[this.side].y !== undefined ? this.sideConfig[this.side].y : this.variantConfig[this.side].y - ShirtMaker3000.selectedShirt['top' + (this.side == 'front' ? 'Front' : 'Back')]
            });

            var wh = {
                width: this.sideConfig[this.side].w !== undefined ? this.sideConfig[this.side].w : this.variantConfig[this.side].w,
                height: this.sideConfig[this.side].h !== undefined ? this.sideConfig[this.side].h : this.variantConfig[this.side].h
            };
            box.css(wh);
            var img = box.find('img');
            img[0].src = this.variantConfig[this.side].project;
            img.css(wh);
        } else {
            //backup
            this.sideConfig[this.side] = this.sideConfig[this.side] || {};
            this.sideConfig[this.side].canvas = JSON.parse(JSON.stringify(this.canvas));
            this.sideConfig[this.side].history = JSON.parse(JSON.stringify(this.history));
            this.sideConfig[this.side].x = box.position().left;
            this.sideConfig[this.side].y = box.position().top;
            this.sideConfig[this.side].w = box.width();
            this.sideConfig[this.side].h = box.height();
            this.sideConfig[this.side].printSize = this.filter.printSize;

            this.side = side;
            this.sideConfig[this.side] = this.sideConfig[this.side] || {};

            //restore
            this.sideConfig[this.side] = this.sideConfig[this.side] || {};
            this.filter.printSize = (this.sideConfig[this.side] || {}).printSize !== undefined ? this.sideConfig[this.side].printSize : this.sideConfig['front'].printSize;
            this.sideConfig[this.side] = this.sideConfig[this.side] || {};
            this.history = JSON.parse(JSON.stringify(this.sideConfig[this.side].history || {}));

            var otherSide = this.side == 'front' ? 'back' : 'front';
            box.css({left: this.sideConfig[this.side].x, top: this.sideConfig[this.side].y});
            var wh = {
                width: this.sideConfig[this.side].w !== undefined ? this.sideConfig[this.side].w : this.sideConfig[otherSide].w,
                height: this.sideConfig[this.side].h !== undefined ? this.sideConfig[this.side].h : this.sideConfig[otherSide].h
            };
            box.css(wh);
            emptyHistory = !Object.keys(this.history).length;
            if (emptyHistory) ShirtMaker3000.restart();

            this.canvas.loadFromJSON(this.sideConfig[this.side].canvas || {}, function () {
                    ShirtMaker3000.canvas.renderAll();
                    ShirtMaker3000.recountPrice();

                    if (ShirtMaker3000.overprint && ShirtMaker3000.overprint.frontImg && ShirtMaker3000.side == 'front') {
                        ShirtMaker3000.canvas.add(ShirtMaker3000.overprint.frontImg);
                        ShirtMaker3000.canvas.bringToFront(ShirtMaker3000.overprint.frontImg);
                        ShirtMaker3000.canvas.renderAll();
                    }
                    if (ShirtMaker3000.overprint && ShirtMaker3000.overprint.backImg && ShirtMaker3000.side == 'back') {
                        ShirtMaker3000.canvas.add(ShirtMaker3000.overprint.backImg);
                        ShirtMaker3000.canvas.bringToFront(ShirtMaker3000.overprint.backImg);
                        ShirtMaker3000.canvas.renderAll();
                    }
                    ShirtMaker3000.overprintEditBoxPosition();
                    ShirtMaker3000.loadShirtSpinner('hide');
                }
            );
        }

        $(this.config.id + ' #gnr_miniatury .gnr_miniatura').each(function () {
            $(this).removeClass('selected');
        });
        $(el).addClass('selected');

        $(this.config.id + ' #gnr_rozmiary #gnr_rozmiary_opcje div').each(function () {
            $(this).removeClass('selected');
        });
        $(this.config.id + ' #gnr_rozmiary #gnr_rozmiary_opcje div.' + this.printSizes[this.filter.printSize].id).addClass('selected');

        this.setBackgroundImage();
        this.resizeEditor(false, false, false);

        this.getSidePrintSize(this.side);
    },

    getPrintSizeInfo: function (printSizeId) {
        for (var i = 0; i < this.printSizeInfo.length; ++i)
            if (this.printSizeInfo[i].id == printSizeId)
                return this.printSizeInfo[i];
    },

    selectPrintSize: function (el, printSizeId, resize, reloadPrice) {
        if ($(el).css('opacity') && $(el).css('opacity') != 1)
            return;

        $(this.config.id + ' #gnr_rozmiary #gnr_rozmiary_opcje div').each(function () {
            $(this).removeClass('selected');
        });
        $(el).addClass('selected');

        var previousOverprint = this.getPrintSizeInfo(this.printSizes[this.filter.printSize].id).overprint;

        for (var i = 0; i < this.printSizes.length; ++i)
            if (this.printSizes[i].id == printSizeId) {
                this.filter.printSize = i;
                break;
            }

        var currentOverprint = this.getPrintSizeInfo(this.printSizes[this.filter.printSize].id).overprint;

        this.updateOverprint(true);
        this.overprintEditBoxPosition();
        this.recountPrice();
        this.setBackgroundImage();

        if (previousOverprint != currentOverprint) {
            var oppositeSide = this.side.match(/front/gi) ? 'back' : 'front';
            if (this.sideConfig[oppositeSide]) {
                var box = $(this.config.id + ' .editBox');
                this.sideConfig[oppositeSide].printSize = this.filter.printSize;
                this.sideConfig[oppositeSide].x = box.position().left;
                this.sideConfig[oppositeSide].y = box.position().top;
                this.sideConfig[oppositeSide].w = box.width();
                this.sideConfig[oppositeSide].h = box.height();
            }
        }

        if (resize === undefined)
            this.resizeEditor(false);
    },

    recountPrice: function () {
        this.price = this.selectedShirt.selectedVariant.basePrice;
        if (!this.isFrontCanvasEmpty())
            this.price += parseFloat(this.printSizes[this.getSidePrintSize('front')].price);
        if (!this.isBackCanvasEmpty())
            this.price += parseFloat(this.printSizes[this.getSidePrintSize('back')].price);
        if (ShirtMaker3000.variant >= 3 && !isNaN(this.variantConfig.markup))
            this.price += parseFloat('0' + this.variantConfig.markup);
        $('#gnr_kwota').html(this.price.format(2).replace('.', ','));
    },

    getRealWidth: function (side) {
        return (side || this.side).match(/front/gi) ? this.selectedShirt.realWidthFront : this.selectedShirt.realWidthBack;
    },

    getRealHeight: function (side) {
        return (side || this.side).match(/front/gi) ? this.selectedShirt.realHeightFront : this.selectedShirt.realHeightBack;
    },

    getWidth: function (side) {
        return (side || this.side).match(/front/gi) ? this.selectedShirt.widthFront : this.selectedShirt.widthBack;
    },

    getHeight: function (side) {
        return (side || this.side).match(/front/gi) ? this.selectedShirt.heightFront : this.selectedShirt.heightBack;
    },

    resizeEditor: function (center, reloadPrice, reposition) {
        var realWidth = this.getRealWidth();
        var realHeight = this.getRealHeight();
        var width = this.getWidth();
        var height = this.getHeight();

        var box = $(this.config.id + ' .editBox');

        var outerWidth = this.side.match(/front/gi) ? this.selectedShirt.widthFront : this.selectedShirt.widthBack;
        var outerHeight = this.side.match(/front/gi) ? this.selectedShirt.heightFront : this.selectedShirt.heightBack;

        //outerbox
        if (this.overprint || this.variant >= 3 && this.variantConfig.overprint == '1')
            $(this.config.id + ' .editMaxArea').css({left: 0, top: 0, width: '100%', height: '100%'});
        else
            $(this.config.id + ' .editMaxArea').css({
                left: ShirtMaker3000.editPositionCorrector.left + parseInt(this.side.match(/front/gi) ? this.selectedShirt.leftFront : this.selectedShirt.leftBack),
                top: ShirtMaker3000.editPositionCorrector.top + parseInt(this.side.match(/front/gi) ? this.selectedShirt.topFront : this.selectedShirt.topBack),
                width: outerWidth,
                height: outerHeight
            });
        this.overprintEditBoxPosition();

        if (ShirtMaker3000.variant == 4) {
            if (this.variantConfig.overprint == '1')
                $(this.config.id + ' .editMaxArea_2').css({left: 0, top: 0, width: '100%', height: '100%'});
            else
                $(this.config.id + ' .editMaxArea_2').css({
                    left: ShirtMaker3000.editPositionCorrector.left + parseInt(this.selectedShirt.leftBack),
                    top: ShirtMaker3000.editPositionCorrector.top + parseInt(this.selectedShirt.topBack),
                    width: outerWidth,
                    height: outerHeight
                });
        }
        if (ShirtMaker3000.variant >= 3) {
            var oryginalPrintWidth = ShirtMaker3000.variantConfig[ShirtMaker3000.side].w;
            var oryginalPrintHeight = ShirtMaker3000.variantConfig[ShirtMaker3000.side].h;
            var newPosition = {
                width: Math.min(oryginalPrintWidth, box.width()),
                height: Math.min(oryginalPrintHeight, box.height())
            };
            box.css(newPosition);
            $(this.config.id + ' .editBox .variant3Image').css(newPosition);

            if ($(this.config.id + " .editBox").is('.ui-resizable')) {
                $(this.config.id + " .editBox").resizable("option", "maxHeight", outerHeight);
                $(this.config.id + " .editBox").resizable("option", "maxWidth", outerWidth);
            }

            var outerBox = $(this.config.id + ' .editMaxArea');
            if (box.position().left + box.width() > outerBox.width()) box.css({left: outerBox.width() - box.width()});
            if (box.position().top + box.height() > outerBox.height()) box.css({top: outerBox.height() - box.height()});
        }

        //editorbox
        var scaleW = width / realWidth;
        var scaleH = height / realHeight;
        var scaleS = this.shirtsMultiplayers[this.filter.size];
        this.filter.scale = scaleW;

        var newWidth = this.printSizes[this.filter.printSize].width * scaleW * scaleS;
        newWidth = newWidth > width ? width : newWidth;

        var newHeight = this.printSizes[this.filter.printSize].height * scaleH * scaleS;
        newHeight = newHeight > height ? height : newHeight;

        var left = box.position().left + newWidth > width ? width - newWidth : box.position().left;
        var top = box.position().top + newHeight > height ? height - newHeight : box.position().top;

        if (this.overprint) {
            newWidth = $('#gnr_koszulka').width();
            newHeight = $('#gnr_koszulka').height();
            left = top = 0;
            $(ShirtMaker3000.config.id + ' .editBoxHandle').addClass('permHide');
        } else {
            $(ShirtMaker3000.config.id + ' .editBoxHandle').removeClass('permHide');
        }
        this.overprintEditBoxPosition();

        this.config.editBoxWidth = newWidth;
        this.config.editBoxHeight = newHeight;

        if (this.variant <= 2) {
            box.css({
                width: newWidth,
                height: newHeight,
                left: left,
                top: top
            });
        }

        if (!this.canvas && this.variant != 3) return;

        //size buttons
        if ($(this.config.id + ' #gnr_rozmiary_opcje')[0] && !$(this.config.id + ' #gnr_rozmiary_opcje')[0].children.length)
            for (var s = 0; s < this.selectedShirt.printSizeId.length; ++s)
                for (var i = 0; i < this.printSizes.length; ++i)
                    if (this.selectedShirt.printSizeId[s] == this.printSizes[i].id)
                        $(this.config.id + ' #gnr_rozmiary_opcje').append('<div class="gnr_roz_opcja tooltip ' + this.printSizes[i].id + '" onclick="ShirtMaker3000.selectPrintSize(this, ' + this.printSizes[i].id + ');" title="Cena: ' + this.printSizes[i].price + 'z\u0142. Maksymalny rozmiar nadruku to ' + this.printSizes[i].width + 'x' + this.printSizes[i].height + 'cm."><strong>' + this.printSizes[i].name + '</div>');

        loadTooltips();

        if (this.variant <= 2) {
            if (center) {
                $(this.config.id + ' .editBox').css({
                    left: (this.selectedShirt['width' + this.side[0].toUpperCase() + this.side.slice(1)] - newWidth) * this.shirtsMultiplayers[this.filter.size] / 2,
                    top: (this.selectedShirt['height' + this.side[0].toUpperCase() + this.side.slice(1)] - newHeight) * this.shirtsMultiplayers[this.filter.size] / 2
                })
            } else {
                try {
                    if (parseInt($(this.config.id + ' .editBox').css('left'), 10) < 0)
                        $(this.config.id + ' .editBox').css({left: 0});
                } catch (e) {
                }
                try {
                    if (parseInt($(this.config.id + ' .editBox').css('top'), 10) < 0)
                        $(this.config.id + ' .editBox').css({top: 0});
                } catch (e) {
                }
            }

            this.canvas.setWidth(newWidth);
            this.canvas.setHeight(newHeight);
            this.canvas.calcOffset();
        }
    },

    selectQuality: function (el, quality) {
        $(this.config.id + ' #gnr_opcje_produkt .selectQuality li').each(function () {
            $(this).removeClass('selected');
        });
        $(el).addClass('selected');
        this.filter.quality = quality;
        this.prepareCategories('type');
    },

    selectKind: function (el) {
        this.unselectKind();
        $(el).addClass('selected');

        this.setFilterKind($(el).html());
        this.loadShirts();
    },

    unselectKind: function (el) {
        $(this.config.id + ' #gnr_opcje_produkt .selectKind li').each(function () {
            $(this).removeClass('selected');
        });
    },

    selectSize: function (el) {
        $(this.config.id + ' #gnr_opcje_produkt .selectSize li').each(function () {
            $(this).removeClass('selected');
        });
        $(el).addClass('selected');
        this.setFilterSize($(el).html());
        this.resizeEditor();
    },

    setTextColor: function (color) {
        var el = ShirtMaker3000.canvas.getActiveObject();
        if (el && el.type === 'text') {
            el.set({fill: color});
            ShirtMaker3000.canvas.renderAll();
        }
    },

    selectColor: function (el) {
        $(this.config.id + ' #gnr_opcje_produkt .gnr_probnik_kolorow li a').each(function () {
            $(this).removeClass('selected');
        });
        $(el).addClass('selected');

        this.setFilterColor($(el).attr('color'));

        for (var i = 0; i < this.selectedShirt.variants.length; ++i)
            if (this.selectedShirt.variants[i].color == this.filter.color)
                this.selectedShirt.selectedVariant = this.selectedShirt.variants[i];

        this.updateOverprint(true);
        this.setBackgroundImage();
        this.updateSideIcons();
        this.recountPrice();
        this.resizeEditor();
        this.loadDescription(++this.loadDescIdx);
    },

    overprintEditBoxPosition: function () {
        if (this.overprint) {
            $(this.config.id + ' .editMaxArea').addClass('overprint');

            $(this.config.id + ' .shirtBackground').css({'background-image': 'none'});
            if (ShirtMaker3000.variant == 4)
                $(this.config.id + ' .shirtBackground_2').css({'background-image': 'none'});
        } else {
            $(this.config.id + ' .editMaxArea').removeClass('overprint');
            this.setBackgroundImage();
        }
    },

    overprintStayAtTop: function () {
        if (this.overprint && this.overprint.frontImg && this.side == 'front') {
            this.canvas.bringToFront(this.overprint.frontImg);
            this.canvas.renderAll();
        }
        if (this.overprint && this.overprint.backImg && this.side == 'back') {
            this.canvas.bringToFront(this.overprint.backImg);
            this.canvas.renderAll();
        }
    },

    updateOverprint: function (force) {
        if (force) {
            if (this.overprint) {
                if (ShirtMaker3000.side == 'front')
                    ShirtMaker3000.getSideCanvas('front').remove(this.overprint.frontImg);
                if (ShirtMaker3000.side == 'back')
                    ShirtMaker3000.getSideCanvas('back').remove(this.overprint.backImg);
                this.overprint = undefined;
            }
        }

        for (var i = 0; i < this.printSizeInfo.length; ++i) {
            if (this.printSizeInfo[i].id == this.printSizes[this.filter.printSize].id) {
                if (this.printSizeInfo[i].overprint == '1') {
                    if (this.overprint)
                        return;

                    this.overprint = {
                        front: this.selectedShirt.selectedVariant.overprint_front,
                        back: this.selectedShirt.selectedVariant.overprint_back
                    };

                    ShirtMaker3000.loadShirtSpinner('show');

                    if (this.overprint.front && !this.overprint.frontImg)
                        fabric.Image.fromURL(this.overprint.front, function (img) {
                            ShirtMaker3000.overprint.frontImg = img;
                            ShirtMaker3000.overprint.frontImg.set({
                                left: 0,
                                top: 0,
                                selectable: false,
                                evented: false
                            });
                            if (ShirtMaker3000.side == 'front') {
                                var canvas = ShirtMaker3000.getSideCanvas('front');
                                canvas.add(ShirtMaker3000.overprint.frontImg);
                                canvas.bringToFront(ShirtMaker3000.overprint.frontImg);
                                canvas.renderAll();
                                ShirtMaker3000.loadShirtSpinner('hide');
                            }
                        });
                    if (this.overprint.back && !this.overprint.backImg)
                        fabric.Image.fromURL(this.overprint.back, function (img) {
                            ShirtMaker3000.overprint.backImg = img;
                            ShirtMaker3000.overprint.backImg.set({
                                left: 0,
                                top: 0,
                                selectable: false,
                                evented: false
                            });
                            if (ShirtMaker3000.side == 'back') {
                                var canvas = ShirtMaker3000.getSideCanvas('back');
                                canvas.add(ShirtMaker3000.overprint.backImg);
                                canvas.bringToFront(ShirtMaker3000.overprint.backImg);
                                canvas.renderAll();
                                ShirtMaker3000.loadShirtSpinner('hide');
                            }
                        });

                    if (this.sideConfig['front'])
                        this.sideConfig['front'].printSize = this.filter.printSize;
                    if (this.sideConfig['back'])
                        this.sideConfig['back'].printSize = this.filter.printSize;
                } else {
                    if (this.overprint) {
                        if (ShirtMaker3000.side == 'front')
                            ShirtMaker3000.getSideCanvas('front').remove(this.overprint.frontImg);
                        if (ShirtMaker3000.side == 'back')
                            ShirtMaker3000.getSideCanvas('back').remove(this.overprint.backImg);

                        if (this.sideConfig['front'])
                            this.sideConfig['front'].printSize = this.filter.printSize;
                        if (this.sideConfig['back'])
                            this.sideConfig['back'].printSize = this.filter.printSize;
                        this.overprint = undefined;
                    }
                }
                break;
            }
        }
        this.overprintEditBoxPosition();
    },

    getOtherSide: function () {
        return this.side == 'front' ? 'back' : 'front';
    },

    selectShirt: function (el) {
        var selectedShirtId = $(el).attr('name').split('_')[1];
        for (var i = 0; i < this.shirts.length; ++i)
            if (this.shirts[i].id == selectedShirtId) {
                this.selectedShirt = this.shirts[i];
                this.selectedShirt.selectedVariant = this.selectedShirt.variants[0];
                break;
            }

        $(this.config.id + ' #gnr_opcje_produkt .gnr_produkty img').removeClass('selected');
        $(el).addClass('selected');
        $(this.config.id + ' #gnr_rozmiary_opcje').html('');

        $(this.config.id + ' #gnr_ikonka_przod span.title').css({display: this.selectedShirt.selectedVariant.back ? 'block' : 'none'});
        $(this.config.id + ' #gnr_ikonka_tyl').css({visibility: this.selectedShirt.selectedVariant.back ? 'visible' : 'hidden'});

        $(this.config.id + ' #gnr_jakosc .quality').css({display: this.selectedShirt.type.match(/quality/gi) ? 'block' : 'none'});
        $(this.config.id + ' #gnr_jakosc .economy').css({display: this.selectedShirt.type.match(/economy/gi) ? 'block' : 'none'});

        if (ShirtMaker3000.variant == 4) {
            $(this.config.id + ' #gnr_jakosc_2 .quality').css({display: this.selectedShirt.type.match(/quality/gi) ? 'block' : 'none'});
            $(this.config.id + ' #gnr_jakosc_2 .economy').css({display: this.selectedShirt.type.match(/economy/gi) ? 'block' : 'none'});
        }

        if (!this.selectedShirt.selectedVariant[this.side])
            this.selectSide(this.getOtherSide(), $('#gnr_ikonka_' + (this.side == 'back' ? 'przod' : 'tyl')));

        this.setFilterShirt();
        this.setBackgroundImage();
        this.updateSideIcons();
        this.recountPrice();
        this.resizeEditor(true);
        this.loadDescription(++this.loadDescIdx);
        $('#gnr_rozmiary_opcje .gnr_roz_opcja:nth-child(1)').click();
    },

    updateSideIcons: function () {
        if (ShirtMaker3000.variant == 4) return;
        $(this.config.id + ' #gnr_ikonka_przod img')[0].src = this.selectedShirt.selectedVariant.front;
        $(this.config.id + ' #gnr_ikonka_tyl img')[0].src = this.selectedShirt.selectedVariant.back;
    },

    setBackgroundImage: function () {
        if (this.overprint) {
            $(this.config.id + ' .shirtBackground').css({'background-image': 'none', 'background-color': '#FFF'});
            if (ShirtMaker3000.variant == 4)
                $(this.config.id + ' .shirtBackground_2').css({'background-image': 'none)', 'background-color': '#FFF'});
        } else {
            $(this.config.id + ' .shirtBackground').css({'background-image': 'url(\'' + this.selectedShirt.selectedVariant[this.side] + '\')', 'background-color': 'transparent'});
            if (this.variant == 3 && this.variantConfig.overprint == '1')
                $(this.config.id + ' .shirtBackground').css({'background-image': 'none', 'background-color': 'transparent'});
            if (ShirtMaker3000.variant == 4)
                $(this.config.id + ' .shirtBackground_2').css({
                    'background-image': 'url(\'' + this.selectedShirt.selectedVariant['back'] + '\')',
                    'background-color': 'transparent'
                });
        }
    },

    prepareCategories: function (update) {
        update = update || 'all';
        switch (update) {
            case 'all':
                this.forWhom = [];
            case 'whom':
                this.shirtKind = [];
            case 'kind':
                this.sizes = [];
                this.colors = [];
            case 'type':
            case 'color':
                this.quality = false;
                this.economy = false;
        }
        var shirts = this.getFilteredShirts();

        for (var i = 0; i < shirts.length; ++i) {
            var shirt = shirts[i];
            if (this.forWhom.indexOf(shirt.subgroup) < 0) this.forWhom.push(shirt.subgroup);
            if (this.shirtKind.indexOf(shirt.group) < 0) this.shirtKind.push(shirt.group);
            for (var s = 0; s < shirt.sizes.length; ++s)
                if (this.sizes.indexOf(shirt.sizes[s]) < 0) this.sizes.push(shirt.sizes[s]);
            for (var s = 0; s < shirt.variants.length; ++s)
                if (this.colors.indexOf(shirt.variants[s].color) < 0) this.colors.push(shirt.variants[s].color);
            if (/economy/gi.test(shirt.type))
                this.economy = true;
            if (/quality/gi.test(shirt.type))
                this.quality = true;
        }

        if (this.selectedShirt) {
            this.colors = [];
            for (var s = 0; s < this.selectedShirt.variants.length; ++s)
                if (this.colors.indexOf(this.selectedShirt.variants[s].color) < 0) this.colors.push(this.selectedShirt.variants[s].color);
            this.sizes = this.selectedShirt.sizes.slice();
        }

        switch (update) {
            case 'whom':
                this.loadShirtKind();
                break;
            case 'kind':
                this.loadShirtType();
                break;
            case 'type':
                this.loadShirts();
                break;
            case 'color':
                this.loadShirtOptions();
                break;
        }
    },

    getFilteredShirts: function () {
        var shirts = [];
        for (var i = 0; i < this.shirts.length; ++i) {
            var shirt = this.shirts[i];
            var regExp = new RegExp(this.filter.quality ? this.filter.quality : 'quality|economy', 'gi');
            if ((shirt.subgroup.indexOf(this.filter.subgroup) >= 0 && this.filter.subgroup || !this.filter.subgroup || this.filter.subgroup.match(/wszystko/gi)) &&
                (shirt.group == this.filter.kind && this.filter.kind || !this.filter.kind || this.filter.kind.match(/wszystko/gi)) &&
                (shirt.type.match(regExp) && this.filter.quality || !this.filter.quality || this.filter.quality.match(/wszystko/gi)))
                shirts.push(shirt);
        }
        return shirts;
    },

    setFilterWhom: function (whom) {
        this.filter.subgroup = whom;
        this.filter.kind = undefined;
        this.filter.color = undefined;
        this.filter.size = undefined;
        this.filter.quality = undefined;
        this.prepareCategories('whom');
    },

    setFilterKind: function (kind) {
        this.filter.kind = kind;
        this.filter.color = undefined;
        this.filter.size = undefined;
        this.filter.quality = undefined;
        this.prepareCategories('kind');
    },

    setFilterShirt: function () {
        this.filter.color = undefined;
        this.filter.size = undefined;
        this.prepareCategories('color');
    },

    setFilterColor: function (color) {
        this.filter.color = color;
    },

    setFilterSize: function (size) {
        this.filter.size = size;
    }
};